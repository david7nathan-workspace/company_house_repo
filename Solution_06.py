# Importing libraries
import numpy as np
import random
import matplotlib.pyplot as plt

# d. Write a program that creates the NumPy array with a leading element of 0.
LEN = 100
a = np.zeros((LEN,), dtype=int)
b = np.zeros((0,), dtype=int)
num = [1, -1]

# Randomly adding 1 element of num array of -1 and 1 to a new array
def GetRandomArr(arr):
    for i in range(LEN):
        arr = np.append(arr, random.choice(num))
    return arr

# e. Fill the table with 100 numbers. Each number should be a sum of
# the previous number and the value drawn from the set (-1, 1).
def GetCumuSum(arr):
    return np.cumsum(arr)

a = GetCumuSum(GetRandomArr(b))
print("Table a with 100 numbers:", a)

# f. Create a graph based on the data in the table.
# data to be plotted
x = np.arange(1, LEN+1)
y = a

# plotting
plt.title("Line graph of cumulative sum [-1, 1]")
plt.xlabel("X axis")
plt.ylabel("Y axis")
plt.plot(x, y, color ="green")
plt.show()

# g. Find minimum and maximum values in the created array.
print("Min value of created array:", min(a), "Max value of created array:", max(a))

# h. Simulate creating an array 50 times and see how many cases have managed to exceed 30 in any given step.
# Illustrate the results on the graph.
INDEX = 50
SUM_MAX_30 = 0
MAX = 0
max_array = np.zeros((0,), dtype=int)
c = np.zeros((0,), dtype=int)
for i in range(INDEX):
    tempArr = GetCumuSum(GetRandomArr(c))
    MAX = max(tempArr)
    print("max(tempArr):", MAX)
    max_array = np.append(max_array, MAX)

    if (max(tempArr) > 30):
        SUM_MAX_30 = SUM_MAX_30 + 1

if (SUM_MAX_30) > 0:
    print("There are ", SUM_MAX_30, "cases have managed to exceed 30")
else:
    print("There is no case has value exceed 30")

# data to be plotted
x = np.arange(1, INDEX+1)
y = max_array
# plotting
plt.title("Line graph of max values of 50 arrays")
plt.xlabel("X axis")
plt.ylabel("Y axis")
plt.plot(x, y, color ="blue")
plt.show()







