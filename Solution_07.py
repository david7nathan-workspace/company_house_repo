# Import pandas
import pandas as pd
pd.set_option('display.max_colwidth', None)
pd.set_option('display.max_columns', None)

# reading csv file
df = pd.read_excel('input.xlsx')

print("1. Staring dataframe: \n", df)

# i. Sort the data by population in descending order.
df = df.sort_values('population', ascending=[False])
df = df.reset_index(drop=True)
print("2. Sorted dataframe: \n", df)

# j. Move the “province capital” column to the last place.
new_cols = [col for col in df.columns if col != 'province capital'] + ['province capital']
df = df[new_cols]
print("3. Moved dataframe: \n", df)

# k. Modify names of provinces so that they begin with a capital letter.
df['province'] = df['province'].str.capitalize()
print("4. First character capitalized dataframe: \n", df)

# l. Create a series object containing names of provinces in the index and a boolean value that indicates whether the average population per km² is greater than 140.
def f(row):
    if row['population'] > 140*row['area [km2]']:
        val = 'Yes'
    else:
        val = 'No'
    return val

df['Average_Population_Per_km_Over_140'] = df.apply(f, axis=1)
print("5. New average population per km2 is greater than 140 dataframe: \n", df)

# m. Delete the row containing „lubuskie” province.
df = df[df.province != "Lubuskie"]
df = df.reset_index(drop=True)
print("6. Delete the row containing „lubuskie” province dataframe: \n", df)

# n. Use pandas.DataFrame.describe() function and show statistical parameters for the analyzed data set.
print("7. Use pandas.DataFrame.describe() function: \n", df)
print(df.describe())
